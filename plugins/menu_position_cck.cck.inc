<?php
/**
 * @file
 * Provides the CCK rule plugin for the Menu Position module.
 */

/**
 * Checks if the selected CCK value is equal to the value on the node.
 *
 * @param $variables
 *   An array containing each of the variables saved in the database necessary
 *   to evaluate this condition of the rule.
 * @return
 *   TRUE if condition applies successfully. Otherwise FALSE.
 */
function menu_position_cck_menu_position_condition_cck($variables) {

  $node = isset($variables['context']['node']) ? $variables['context']['node'] : NULL;

  if (isset($node->{$variables['field']})) {
    $value = $node->{$variables['field']}[0]['value'];
    return $value == $variables['value'];
  }
  return false;
}

/**
 * Adds form elements for the cck plugin to the rule configuration form.
 *
 * @param $form
 *   A reference to the "add/edit rule" form array. New form elements should be
 *   added directly to this array.
 * @param $form_state
 *   A reference to the current form state.
 */
function menu_position_cck_menu_position_rule_cck_form(&$form, &$form_state) {

  $variables = !empty($form_state['#menu-position-rule']['conditions']['cck']) ? $form_state['#menu-position-rule']['conditions']['cck'] : array();

  $form['conditions']['cck'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('CCK'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#attached'    => array(
      'js' => array(drupal_get_path('module', 'menu_position') . '/plugins/menu_position.example_plugin.js'),      // Ensures a proper summary is added to its vertical tab.
    ),
  );

  $rawFields = content_fields(null, '');
  $cckFields = array(-1 => '<none>');
  foreach ($rawFields as $key => $field) {
    $cckFields[$key] = $field['widget']['label'];
  }

  $form['conditions']['cck']['cck_field'] = array(
    '#type'          => 'select',
    '#title'         => t('CCK Field'),
    '#default_value' => !empty($variables['field']) ? $variables['field'] : -1,
    '#description  ' => t('The cck field'),
    '#options'       => $cckFields,
    '#weight'        => 10,
  );
  $form['conditions']['cck']['cck_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('CCK value'),
    '#default_value' => !empty($variables['value']) ? $variables['value'] : '',
    '#description  ' => t('The cck field'),
    '#weight'        => 20,
  );

  $form['#submit'][] = 'menu_position_cck_menu_position_rule_cck_form_submit';
}

/**
 * Prepares the plugin's variables to be stored in the rule.
 *
 * @param $form
 *   A reference to the "add/edit rule" form array.
 * @param $form_state
 *   A reference to the current form state, including submitted values.
 */
function menu_position_cck_menu_position_rule_cck_form_submit(&$form, &$form_state) {
  
  if (!empty($form_state['values']['cck_field'])) {
    if ($form_state['values']['cck_field'] && $form_state['values']['cck_field'] != -1 && $form_state['values']['cck_value']) {
      $variables = array(
        'field' => $form_state['values']['cck_field'],
        'value' => $form_state['values']['cck_value'],
      );
      $form_state['values']['conditions']['cck'] = $variables;
    }
  }
}
